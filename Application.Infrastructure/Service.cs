﻿using Application.Core.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Application.Infrastructure
{
    public class Service
    {

        public IEnumerable<Products> Get()
        {
            using(var context = new CoffeeMugTaskContext())
            {
                return context.Products.ToList();
            }
        }
    }
}
